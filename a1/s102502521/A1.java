package ce1002.a1.s102502521;
import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);    //新增一個scanner      
		
		int innum=0;    //設定變數
		
		do{
			System.out.print("Please input a number (5~30): ");
			innum=sc.nextInt();
    	
			if(innum>30||innum<5){
				System.out.println("Out of range!");    //判斷值是否合理
			}
    	
		} while (innum>30||innum<5);
    
		for(int y=1;y<=innum;y++){    //印出圖形
			for(int x=1;x<=innum;x++){
				if(y%2==1){
					if(x%2==1){
						System.out.print("X");
					}
					else{
						System.out.print("O");
					}
				}
				else{
					if(x%2==1){
						System.out.print("O");
					}		
					else{
						System.out.print("X");
					}
				}
			}
			System.out.println();
		}
	}
	
}
