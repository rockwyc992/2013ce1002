package ce1002.a1.s995002046;


import java.util.Scanner;


class A1{
	
	public static void main(String[] args) { 
		int num=0;
		Scanner scanner;
		System.out.print("Please input a number (5~30)\n");
		while(5>3){
			scanner = new Scanner(System.in);
			num=scanner.nextInt();
			if(5 <= num && num <= 30){
				for(int i=1; i<=num; i++){
					for(int j=1; j<=num; j++){
						if((i+j)%2==0)
							System.out.print("X");
						else
							System.out.print("O");
					}
					System.out.print("\n");
				}
				break;
			}
			else
				System.out.print("Out of range ! Please input again(5~30):\n");
			
		}
		scanner.close();
	}
}