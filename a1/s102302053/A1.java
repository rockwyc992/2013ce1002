package ce1002.a1.s102302053;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class A1 {

	public static void main(String[] args) throws IOException {
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));//儲存輸入的數字
		System.out.print("Please input a number (5~30): ");
		String str=buf.readLine();//輸入
		int num =Integer.parseInt(str);//將存到的字串轉為int
		
		while (num < 5 || num >30){//檢查輸入是否有在範圍內,如果沒有,要求使用者重新輸入
			System.out.print("Out of range!\r\nPlease input a number (5~30): ");
			str=buf.readLine();//輸入
			num =Integer.parseInt(str);//將存到的字串轉為int
		}
		
		for(int tem1 = 0; tem1 < num; tem1++){//從第一行開始,由上而下繪製圖形
			for(int tem2 = 0; tem2 < num; tem2++){//有左而右輸出xo
				if (tem1%2 ==0){//奇數行由x開始輸出
					if(tem2%2 == 0){//奇數列輸出x
						System.out.print("x");
					}
					else if (tem2%2 == 1){//偶數列輸出o
						System.out.print("o");
					}
				}
				if (tem1%2 == 1){//偶數行由o開始輸出
					if(tem2%2 == 0){//奇數列輸出o
						System.out.print("o");
					}
					else if (tem2%2 == 1){//偶數列輸出x
						System.out.print("x");
					}
				}
			}
			System.out.println("");//換行
			}	
		}
}

