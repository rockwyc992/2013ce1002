package ce1002.A1.s102502046;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		int a; //宣告一個整數
		System.out.print("Please input a number (5~30): "); //輸出一串字
		Scanner in = new Scanner (System.in); //輸入
		a = in.nextInt(); 
		while(a<5||a>30)
		{
			System.out.println("Out of range!");
			System.out.print("Please input again(5~30): ");
			a = in.nextInt();
		}
		for (int i=1 ; i<=a ; i++) //印出結果
		{
			for(int j=1 ; j<=a ; j++)
			{
				if((i+j)%2==0)
					System.out.print("X");
				else
					System.out.print("O");
			}
			System.out.println("");
		}
	}

}
