package ce1002.a1.s102502536;

import java.util.Scanner;

public class a1 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);  //creat a scanner object
		
		System.out.print("Please input a number (5~30): ");  //輸出字串
		int x = input.nextInt();  //設變數
		
		while(x > 30 || x < 5) {  
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			x = input.nextInt();
		}  //設輸入範圍
		
		int a = 0; //設變數
		int b = 0;
		int n = 0;
	
		for(a = 0;a < x;a++) {
            
			if(a == 2*n) {
				
				int m = 0;  
				
                for(b = 0;b < x;b++) {
					
					if(b == 2*m) {
					    System.out.print("X");
					}
					if(b == 2*m+1) {
						System.out.print("O");
						
						m++;
					}
					
				}
			}
			if(a == 2*n+1) {
				
				int o = 0;
				
                for(b = 0;b < x;b++) {
					
					if(b == 2*o) {
					    System.out.print("O");
					}
					if(b == 2*o+1) {
						System.out.print("X");
						
						o++;
					}
					
				}
                
                n++;
			}
			
			System.out.print("\n");
			b = 0;
			
		}  //輸出矩形
		
	}

}
