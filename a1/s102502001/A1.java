package ce1002.a1.s102502001;
import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Please input a number (5~30): "); //顯示字串
		Scanner cin=new Scanner(System.in);
		int num=cin.nextInt() ;  //輸入數字
		
		while (num<5||num>30) {  //判斷範圍
			System.out.print("Out of range!\r\nPlease input again (5~30): ");
			num=cin.nextInt() ;
		}
		int x=1;
		for(int y=1;y<=num;y++){
			for(;x<=num;x++){  //讓XO交叉顯示
				if(y%2==0){
				if(x%2==0){
					System.out.print("X");
				}
				else
					System.out.print("O");
				}
				else{
					if(x%2==1){
						System.out.print("X");
					}
					else
						System.out.print("O");
					}
			}
			x=1;     //X回歸0
			System.out.print("\r\n");  //換行
		}
	}
}
