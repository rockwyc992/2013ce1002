package ce1002.a1.s102502538;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int x;
		System.out.print("Please input a number (5~30): ");
		x = scanner.nextInt();
		while (x < 5 || x > 30)	{//判斷x的範圍
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			x = scanner.nextInt();
		}
		for (int i = 0; i < x; i++) {//設定要輸出的符號
			for (int t = 0; t < x; t++){
				if (t % 2 == 0 && i%2 ==0) {
					System.out.print("X");
				}
				else if(t % 2 == 1 && i%2 ==0){
					System.out.print("O");
				}	
				else if(t % 2 == 0 && i%2 ==1){
					System.out.print("O");
				}	
				else{
					System.out.print("X");
				}
			}
			System.out.println();//換行
		}
	}
}
