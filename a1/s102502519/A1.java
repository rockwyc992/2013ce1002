package ce1002.a1.s102502519;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int num1=0;
		
		System.out.print("Please input a number (5~30): ");
		num1=input.nextInt();
		
		while(num1<5 || num1>30)    //將範圍限制在5~30
		{
				System.out.println("Out of range!");
				System.out.print("Please input again (5~30): ");
				num1=input.nextInt();	
		}
		
		for(int x=1;x<=num1;x++){    //做出n*n的方形
			for(int y=1;y<=num1;++y){
				if(x%2==1){
					if(y%2==1)
						System.out.print("X");
					else
						System.out.print("O");
				}
				else{
					if(y%2==1)
						System.out.print("O");
					else
						System.out.print("X");
				}
					
			}
			System.out.println();    //換行
		}
		
	}

}
