package ce1002.a1.s101201046;

import java.util.Scanner;

public class A1 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner (System.in); 
		int num, i;
		String str = "XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOX"; 
		

		System.out.print("Please input a number (5~30): ");
		num = scanner.nextInt();
		
		while (num > 30 || num < 5) {
			System.out.print("Out of range!\n");
			System.out.print("Please input again (5~30): ");
			num = scanner.nextInt();
		}
		
		for (i = 0; i < num; i++) 
			if ((i%2) == 1)
				System.out.println(str.substring(1, (num+1)));
			else
				System.out.println(str.substring(0,num));
		
		return;
	}
}
