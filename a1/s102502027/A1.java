package ce1002.a1.s102502027;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
	   int num;
	System.out.print("Please input a number (5~30): ");
	num= input.nextInt();
	
	while(num>30 || num <5){
		System.out.println("Out of range!");
	    System.out.print("Please input again (5~30): ");
	    num= input.nextInt();
	}
	
	for(int x=0;x<=num;x++)
	{
		for(int y=0;y<=num;y++)
		{
			if(y%2==1)
			{
				if(x%2==1)
					System.out.print("X");
				else
					System.out.print("O");
			}
			else
			{
				if(x%2==1)
					System.out.print("O");
				else
					System.out.print("X");
			}
		}
		System.out.println();
	}
	
  }
}
