package ce1002.A1.s102502047;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		System.out.print("Please input a number (5~30): ");
		Scanner i = new Scanner(System.in);
		int n=i.nextInt();
		for(;n>30||n<5;)
		{
			System.out.print("Out of range ! \n" +"Please input again(5~30): ");
			n=i.nextInt();
		}	
		for(int y=1;y<=n;y++)
		{
			if(y%2==1)
				for(int x=1;x<=n;x++)
				{	
					if(x%2==1)
						System.out.print("X");
					if(x%2==0)
						System.out.print("O");
				}
			if(y%2==0)
				for(int x=1;x<=n;x++)
				{	
					if(x%2==1)
						System.out.print("O");
					if(x%2==0)
						System.out.print("X");
				}
			System.out.print("\n");
		}
		
	}

}