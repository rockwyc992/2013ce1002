package ce1002.a1.s102502516;
import java.util.Scanner; //import scanner

public class A1 {

	public static void main(String[] args) 	{
		
		int userInput;
		Scanner scanner = new Scanner(System.in); 
		System.out.print("Please input a number (5~30): ");
		userInput = scanner.nextInt(); //第一次輸入
			
		while(userInput < 5 || userInput > 30){
			System.out.print("Out of range!\nPlease input again (5~30): ");	//不符合規定的輸入
			userInput = scanner.nextInt();
		}
		boolean status = false; 	//用來控制目前是O還是X
		for (int i = 0; i < userInput; i++) {
			for (int j = 0; j < userInput; j++) {	//兩層迴圈畫出棋盤
				if (status == false){
					System.out.print("X");	//依題目，第一個是X
					status = !status;	//畫完後改變狀態以便下次繪製
				}
				else {
					System.out.print("O");	//若為true則畫圈
					status = !status;
				}
			}
			System.out.println();   //換行
			if ( userInput % 2 == 0){  //若是偶數長度，則必須改變狀態以免與下一行圖形重複
			status = !status;
			}
		}
	}

}
