package ce1002.a1.s102502042;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);	//create a scanner
		System.out.print("Please input a number (5~30): ");
		int number = input.nextInt();
		while(number<5 || number>30)	//wrong input
		{
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			number = input.nextInt();
		}
		//output
		for(int i=1;i<=number;++i)
		{
			for(int j=1;j<=number;++j)
			{
				if((i+j-1)%2==1)System.out.print('X');
				else  System.out.print('O');
			}
			System.out.println("");
		}
	}

}
