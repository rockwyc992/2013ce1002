package ce1002.A1.s102502542;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int c;// 宣告變數
		System.out.print("Please input a number (5~30): ");// 輸出
		c = scanner.nextInt();// 輸入
		while (c < 5 || c > 30) {
			System.out.print("Out of range!");
			System.out.print("\n");
			System.out.print("Please input again (5~30): ");
			c = scanner.nextInt();
		}// 當c<5或c>30進入迴圈
		for (int b = 1; b <= c; b++) {
			if (b % 2 == 0) {
				for (int d = 1; d <= c; d++) {
					if (d % 2 == 0)
						System.out.print("X");
					else if (d % 2 == 1)
						System.out.print("O");
				}
			} else if (b % 2 == 1) {
				for (int d = 1; d <= c; d++) {
					if (d % 2 == 0)
						System.out.print("O");
					else if (d % 2 == 1)
						System.out.print("X");
				}
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}// 棋盤的迴圈打法
		// TODO Auto-generated method stub

}
