package ce1002.a1.s102502550;
import java.util.Scanner;
public class a1 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);      //輸入物件宣告
		int a=0;
		
		do{
		   System.out.print("Please input a number (5~30): ");
		   a=in.nextInt();
		   
		   if(a<5 || a>30)
			   System.out.print("Out of range!\n");   //超出範圍則輸出提示
		   
		}while(a<5 || a>30);
		
		for(int i=1;i<=a;i++){                //繪製圖案
		   for(int j=1;j<=a;j++){
			   
			   if( (i+j) % 2 == 0){
				   System.out.print("X");
			   }
			   else{
				   System.out.print("O");
			   }
			   
		   }
		   System.out.println("");
		}
	}

}
