package ce1002.a1.s102502508;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input= new Scanner(System.in);    //進入程式
		int k=0 ;
		int i=0 ;
		int j=0 ;
		System.out.print("Please input a number (5~30): "); //輸出在螢幕上
		do{
			
			k=input.nextInt();       //從鍵盤輸入到電腦裡         
			if(k<5 || k>30)
			{
				System.out.println("Out of range!");
				System.out.print("Please input number again(5~30): ");
				
			}
			
		  }while(k<5 || k>30);
		for(i=1 ; i<=k ; i=i+1)   //利用迴圈畫圖 
		{
			for(j=1 ; j<=k ; j=j+1)
			{
				if((i+j)%2==0)
				{
					System.out.print("X");   
				}
				else if((i+j)%2!=0)
				{
					System.out.print("O");
				}
			}
			System.out.println("");      //換行  
		}

	}

}
