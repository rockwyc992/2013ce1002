package ce1002.a1.s102502531;

import java.util.*;

public class A1 {
	public static void main(String[] args) {
		int a;
		int b;
		do {
			System.out.print("Please input a number (5~30): "); //輸入數值
			Scanner number = new Scanner(System.in);
			a = number.nextInt();
			if (a < 5 || a > 30) {
				System.out.println("Out of range!"); //錯誤輸入
			}
		} while (a < 5 || a > 30);
		b = a;
		for (int i = 0; i < a; i++) {  //n*n矩陣
			for (int y = 0; y < b; y++) {
				if (i % 2 == 1) {
					if (y % 2 == 1) {
						System.out.print("X");  //兩種情形
					} else if (y % 2 == 0) {
						System.out.print("O");
					}
				}
				if (i % 2 == 0) {
					if (y % 2 == 1) {
						System.out.print("O");
					} else if (y % 2 == 0) {
						System.out.print("X");
					}
				}
			}
			System.out.println();
		}

	}
}
