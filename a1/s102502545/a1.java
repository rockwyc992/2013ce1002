package ce1002.a1.s102502545;

import java.util.Scanner;

public class a1 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int number = 0;//設定變數
		do {//利用迴圈限定大小
			System.out.print("Please input a number (5~30): ");
			number = input.nextInt();
			if (number < 5 || number > 30) {
				System.out.println("Out of range!");
			}
		} while (number < 5 || number > 30);

		for (int i = 0; i < number; i++) {
			switch (i % 2) {//利用switch迴圈寫出兩種情況
			case 0:
				for (int j = 0; j < number; j++) {
					if (j % 2 == 0)
						System.out.print("X");
					else
						System.out.print("O");
				}
				System.out.println();
				break;
			case 1:
				for (int j = 0; j < number; j++) {
					if (j % 2 == 0)
						System.out.print("O");
					else
						System.out.print("X");
				}
				System.out.println();
				break;
			}
		}
	}
}
