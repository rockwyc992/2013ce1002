package ce1002.a1.s101201521;
import java.util.Scanner;
public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in); //declare input
		int num;//declare num
		System.out.print("Please input a number (5~30): ");
		num = input.nextInt();
		
		while(num>30 || num<5) {//check num and request again 
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			num = input.nextInt();
		}
		
		for(int i=1; i<=num; i++) {
			for(int j=1; j<=num; j++) {
				if((i+j)%2==0)//i+j even, print X
					System.out.print("X");
				else//i+j odd, print O
					System.out.print("O");
			}
			System.out.println("");// next line
		}
	}
}
