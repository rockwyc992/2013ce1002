package ce1002.a1.s101201504;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Please input a number (5~30): ");
		int n=sc.nextInt();
		while (n<5 || n>30)
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (5~30): ");
			n=sc.nextInt();
		}
		for (int i=0;i<n;i++)
		{
			for (int j=0;j<n;j++)
			{
				if ((i+j) % 2==0)
				{
					System.out.print("X");
				}
				else
				{
					System.out.print("0");
				}
			}
			System.out.println();
		}
	}
}
