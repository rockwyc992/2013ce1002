package ce1002.a1.s102502558;
import java.util.Scanner;
public class A1 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 先讀取一次數字
		Scanner input = new Scanner(System.in);
		System.out.print("Please input a number (5~30): ");
		int num = input.nextInt();
		
		// 如果超出範圍就繼續讀取
		while( num<5||num>30 ) 
		{	  
			System.out.println("Out of range!");
			System.out.print("Please input a number (5~30): ");
			num = input.nextInt();
		}
		// 劃出OX
		for(int i=0 ; i<num ;i++)
		{
			for(int j=0 ;j<num;j++ )
			{
				if((i+j)%2==0)
					System.out.print("X");
				else
					System.out.print("O");
			}
		    System.out.print("\n"); // 換行
		}	
		
		input.close();
	}
}
