package ce1002.e2.s102502517;

import java.util.Scanner; //輸入用
import java.util.Random; //隨機數

public class E2 {

	public static void main(String[] args) {
		int f_number = 0;
		int s_number = 0;
		Scanner scanner = new Scanner(System.in);

		do
		{
			System.out.print("Please input first number (3~10): ");
			f_number = scanner.nextInt(); //輸入列數
			if(f_number<3 || f_number>10)
				System.out.println("Out of range!");
		}
		while(f_number<3 || f_number>10);
		
		do
		{
			System.out.print("Please input second number (3~10): ");
			s_number = scanner.nextInt(); //輸入行數
			if(s_number<3 || s_number>10)
				System.out.println("Out of range!");
		}
		while(s_number<3 || s_number>10);
		
		int[][] array = new int[11][11]; //宣告二維陣列
		Random rand = new Random();
		
		for(int i=1; i<=f_number; i++) //輸出圖案
		{
			for(int j=1; j<=s_number; j++)
			{
				array[i][j] = rand.nextInt(2); //隨機產生0或1
				System.out.print(array[i][j] + " ");
			}
			System.out.println(); //換行
		}
		
		int counter = 0; //宣告計數器
		
		for(int k=1; k<=s_number; k++) //檢查是否連續
		{
			for(int m=1; m<=f_number; m++)
			{
				if(k!=s_number) //若在邊界則不檢查
				{
					if(array[m][k]==array[m][k+1] && array[m][k]==1)
						counter++;
				}
				
				if(m!=f_number) //若在邊界則不檢查
				{
					if(array[m][k]==array[m+1][k] && array[m][k]==1)
						counter++;
				}
			}
		}
		
		System.out.print("total: " + counter);
	}

}
