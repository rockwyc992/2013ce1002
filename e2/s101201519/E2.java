package ce1002.e2.s101201519;
import java.util.Scanner;
import java.util.Random;
import java.util.Date;
public class E2 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);//自訂一個使用System.in的Scanner物件
		int[][] x= new int[10][10];//宣告一個二維的陣列
		int l=0,w=0;
		int times=0;//用來計算次數
		while(l<3 || l>10)//設定陣列大小
		{
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();
			if(l<3 || l>10)
			{
				System.out.println("Out of range!");
			}
		}
		while(w<3 || w>10)
		{
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();
			if(w<3 || w>10)
			{
				System.out.println("Out of range!");
			}
		}
		input.close();
		
		java.util.Date seed = new java.util.Date();//設種子
		Random rand = new Random(seed.getTime());//以時間調整變數
		for(int i=0; i<l; i++)//先將0,1填入至陣列
		{
			for(int j=0; j<w; j++)
			{
				x[i][j] = (int)(Math.random()*2);//隨機變數的範圍設定在0~1
				System.out.print(x[i][j]);
				System.out.print(" ");
			}
			System.out.println("");//換行
			
		}
		for(int i=0; i<l; i++)//計算連續1的次數 先一列一列比對
		{
			for(int j=0; j<w; j++)
			{
				if(x[i][j]==1 && x[i][j+1]==1)
					times++;
			}
		}
		for(int j=0; j<w; j++)//一行一行比對
		{
			for(int i=0; i<l; i++)
			{
				if(x[i][j]==1 && x[i+1][j]==1)
					times++;
			}
		}
		System.out.println("total: "+times);	
	}
}
