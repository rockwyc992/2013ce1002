package ce1002.e2.s101201506;
import java.util.Scanner;
import java.util.Random; // import 隨機變數用

public class E2 {
	private static Scanner scanner;
	
	public static void main(String[] args) 
	{
		scanner = new Scanner(System.in);
		Random ran = new Random();
		
		int a=0; //所輸入的a行
		int b=0; //所輸入的b列
		
		do
		{
			System.out.print("Please input first number (3~10): ");
			a = scanner.nextInt();
			if( a<3 || a>10 )
				System.out.println("Out of range!");
		}while( a<3 || a>10 );
		
		do
		{
			System.out.print("Please input second number (3~10): ");
			b = scanner.nextInt();
			if( b<3 || b>10 )
				System.out.println("Out of range!");
		}while( b<3 || b>10 );
		
		int[][] x; //做個 a*b 的陣列
		x = new int[a][b];
		
		for( int i=0 ; i<a ; i++ )
		{
			for( int j=0 ; j<b ; ++j )
				x[i][j] = ran.nextInt(2);	
		}
		
		int total=0 ;
		for( int i=0 ; i<a ; ++i )
		{
			for( int j=0 ; j<b ; ++j )
			{
				System.out.print(x[i][j] + " ");
				
				if(j != b-1)
					if(x[i][j] == 1 && x[i][j + 1] == 1)					
						++total;
				
				if(i != a-1)
					if(x[i][j] == 1 && x[i + 1][j] == 1)
						++total;
					
			}
			System.out.println();
		}
		System.out.println("total: " + total);
	}
}
	
	
