package ce1002.e2.s102502046;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		int i=0,w=0,n=0; //宣告行數列數，以及計數器
		Random ran = new Random();	//
		System.out.print("Please input first number (3~10): ");
		Scanner in = new Scanner (System.in); //輸入
		i = in.nextInt();
		while(i<3||i>10)	//判斷是否在範圍內
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			i = in.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		w = in.nextInt();
		while(w<3||w>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			w = in.nextInt();
		}
		int[][] arr = new int[i][w] ;	//宣告陣列
		for(int x=0 ; x<i ; x++)
		{
			for(int y=0 ; y<w ; y++)
			{
				arr[x][y]=ran.nextInt(2);
				System.out.print(arr[x][y]+" ");
			}
			System.out.println("");
		}
		for(int x=0 ; x<i ; x++)		//判斷是否連續 1
			for(int y=0 ; y<w-1 ; y++)
				if(arr[x][y]==1&&arr[x][y+1]==1)
					n++;
		for(int x=0 ; x<i-1 ; x++)
			for(int y=0 ; y<w ; y++)
				if(arr[x][y]==1&&arr[x+1][y]==1)
					n++;
		System.out.print("total: ");
		System.out.print(n);
	}
}