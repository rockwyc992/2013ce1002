package s102502548;

import java.util.Scanner ;
import java.util.Random ;

public class E2 {

	public static void main(String[] args) {
		int number1=0 ;//宣告變數、陣列
		int number2=0 ;
		int[][] arg=new int[10][10] ;
		int total=0 ;
		
		while (true){//使用迴圈來重複輸入輸出
			System.out.print("Please input first number (3~10): ") ;
		
			Scanner sc=new Scanner(System.in) ;
			
			number1=sc.nextInt() ;
			
			if (number1<3||number1>10){
				System.out.println("Out of range!") ;
			}
			else break ;
		}
		
		while (true){
			System.out.print("Please input second number (3~10): ") ;
		
			Scanner sc=new Scanner(System.in) ;
			
			number2=sc.nextInt() ;
			
			if (number2<3||number2>10){
				System.out.println("Out of range!") ;
			}
			else break ;
		}
		
		Random ran=new Random() ;//隨機變數
		
		for (int x=0;x<number1;x++){//用迴圈來重複存取隨機辯輸到陣列中
			for (int y=0;y<number2;y++){
				arg[x][y]=ran.nextInt(2) ;
				
				System.out.print(arg[x][y]) ;
				System.out.print(" ") ;
			}
			System.out.println() ;
		}
		
		for (int a=0;a<number1;a++){//判斷相連的兩個樹是不是都是1
			for (int b=0;b<number2;b++){
				if (arg[a][b]==1&&arg[a][b]==arg[a][b+1]){
					total=total+1 ;
				}
			}
		}
		
		for (int a=0;a<number1;a++){
			for (int b=0;b<number2;b++){
				if (arg[a][b]==1&&arg[a][b]==arg[a+1][b]){
					total=total+1 ;
				}
			}
		}
		
		System.out.print("total: ") ;//輸出total
		System.out.print(total) ;
	}

}
