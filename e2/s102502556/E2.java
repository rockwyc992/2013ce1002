package ce1002.e2.s102502556;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		int num1 = 0; //宣告一個int變數 num1，用來儲存一共有幾列
		int num2 = 0; //宣告一個int變數num2，用來儲存一共有幾行
		System.out.print("Please input first number (3~10): ");
		Scanner input = new Scanner(System.in); //宣告一個Scanner
		num1 = input.nextInt(); //讓使用者輸入num1
		while ( num1 < 3 || num1 > 10 ) //檢查輸入是否合乎標準，否則要求其重新輸入
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			num1 = input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		num2 = input.nextInt(); //讓使用者輸入num2
		while ( num2 < 3 || num2 > 10 ) //檢查輸入是否合乎標準，否則要求其重新輸入
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			num2 = input.nextInt();
		}
		input.close(); //關閉Scanner
		int[][] array = new int[num1][num2]; //宣告一個名為array的int二維陣列
		Random rand = new Random(); //宣告一個Random
		for ( int i = 0 ; i < num1 ; i++ ) //讓array的每個元素都隨機產生1或0，產生的同時輸出整個陣列
		{
			for ( int j = 0 ; j < num2 ; j++ )
			{
				array[i][j] = rand.nextInt(2);
				System.out.print(array[i][j]+" ");
			}
			System.out.println("");
		}
		int total = 0; //宣告一個int變數total，用來儲存相連的1的總數
		for ( int k = 0 ; k < num1 ; k++ ) //計算水平連續的1
		{
			for ( int l = 0 ; l < num2 - 1 ; l++ )
			{
				if ( array[k][l] + array[k][l+1] == 2 )
				{
					total += 1;
				}
			}
		}
		for ( int m = 0 ; m < num2 ; m++ ) //計算垂直連續的1
		{
			for ( int n = 0 ; n < num1 - 1 ; n++ )
			{
				if ( array[n][m] + array[n+1][m] == 2 )
				{
					total += 1;
				}
			}
		}
		System.out.print("total: "+total); //輸出結果
	}
}
