package ce1002.e2.s101201003;

import java.util.Scanner;

public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner number=new Scanner(System.in);//宣告Scanner類別的物件
		int l,w;				
		do{
			System.out.print("Please input first number (3~10): ");
			l=number.nextInt();
			if(l<3||l>10)
				System.out.println("Out of range!");
		}while(l<3||l>10);//問欲顯示的長,如果輸入範圍錯誤就重新輸入
		do{
			System.out.print("Please input second number (3~10): ");
			w=number.nextInt();
			if(w<3||w>10)
				System.out.println("Out of range!");
		}while(w<3||w>10);//問欲顯示的寬,如果輸入範圍錯誤就重新輸入
		
		
		int[][] x;//宣告二微陣列
        x = new int[10][10];
        for(int i=0;i<l;i++){
        	for(int j=0;j<w;j++){
        		 x[i][j] = (int) (Math.random() * 2);//亂數的範圍是0~1
        		 System.out.print(x[i][j]);//輸出陣列
        		 System.out.print(" ");
        	}
        	System.out.print("\n");
        }
        int times=0;//計算次數
        for(int i=0;i<l;i++){
        	for(int j=0;j<w-1;j++){
        		if(x[i][j]+x[i][j+1]==2)
        			times++;//如果相鄰相加等於2，次數加1        	
        	}
        }
        for(int j=0;j<w;j++){
        	for(int i=0;i<l-1;i++){
        		if(x[i][j]+x[i+1][j]==2)
        			times++;//如果相鄰相加等於2，次數加1        	
        	}
        }
        System.out.print("total: "+times);//輸出次數
        number.close();  
        }
}
