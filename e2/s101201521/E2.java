package ce1002.e2.s101201521;
import java.util.Scanner;
import java.util.Random;
import java.util.Date;
public class E2 {
	
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int[][] table=new int[10][10];
		int l=0, w=0;
		//request table width
		do{
			System.out.print("Please input first number (3~10): ");
			l=input.nextInt();
			if(l<3 || l>10)
				System.out.println("Out of range!");
		} while(l<3 || l>10);
		//request table length
		do{
			System.out.print("Please input second number (3~10): ");
			w=input.nextInt();
			if(w<3 || w>10)
				System.out.println("Out of range!");
		} while(w<3 || w>10);
		input.close();
		//construct and print table
		java.util.Date seed = new java.util.Date();
		Random rand = new Random(seed.getTime());
		int total=0;
		for(int i=0; i<l; i++){
			for(int j=0; j<w; j++){
				table[i][j]=rand.nextInt(2);
				System.out.print(table[i][j]+" ");
			}
			System.out.println();
		}
		//count continuous 1's
		for(int i=0; i<l; i++){
			for(int j=0; j<w; j++){
				if(j<w && table[i][j]+table[i][j+1]==2)
					total++;
				if(i<l && table[i][j]+table[i+1][j]==2)
					total++;
			}
		}
		//print total number of cotinuous 1's
		System.out.println("total: "+total);
		
	}
}
