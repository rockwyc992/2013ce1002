package ce1002.e2.s102502528;
import java.util.Scanner;
import java.util.Random;

public class E2 {
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		Random ran = new Random();
		int h ,w ,tol = 0;
		int[][] arr= new int[11][11] ;
		do                    //input height
		{
			System.out.println("Please input first number (3~10): ");
			h = scanner.nextInt();
			if(h < 3 || h > 10)
			{
				System.out.println("Out of range!");
			}
		}while(h < 3 || h > 10);
		do        //input width
		{
			System.out.println("Please input second number (3~10): ");
			w = scanner.nextInt();
			if(w < 3 || w > 10)
			{
				System.out.println("Out of range!");
			}
		}while(w < 3 || w > 10);
		
		for(int i = 0; i < h; i++)    //random
		{
			for(int j = 0; j < w; j++)
			{
				arr[i][j] = ran.nextInt(2);
				System.out.print(arr[i][j] + " ");
			}
			System.out.println("");
		}
		for(int i = 0; i < h; i++)   //judge if there is 1
		{
			for(int j = 0; j < w; j++)
			{
				if(arr[i][j] == 1 && arr[i+1][j] == 1)
					tol += 1;
				if(arr[i][j] == 1 && arr[i][j+1] == 1)
					tol += 1;
			}
		}
		System.out.println("total: " + tol);  //output
	}

}
