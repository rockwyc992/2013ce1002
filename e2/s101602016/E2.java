package ce1002.e2.s101602016;
import java.util.Scanner;
import java.util.Random;

public class E2 {
	private static Scanner scanner;
	 
	public static void main(String[] args){
		int x=0,y=0,r=0,n=0;//長，寬，亂數，連續1的數量
		int[][] z;//儲存亂數的陣列
        z = new int[10][10];//設定陣列大小
		scanner=new Scanner(System.in);
		Random ran = new Random();
		do{//請使用者輸入第一個數字，如果不符合範圍則重新輸入
		System.out.print("Please input first number (3~10): ");
		x=scanner.nextInt();//儲存在x
		if(x<3||10<x)
			System.out.print("Out of range!\n");
		}while(x<3||10<x);
		do{//請使用者輸入第二個數字，如果不符合範圍則重新輸入
		System.out.print("Please input second number (3~10): ");
		y=scanner.nextInt();//儲存在y
		if(y<3||10<y)
			System.out.print("Out of range!\n");
		}while(y<3||10<y);
		for(int i=0;i<x;i++){
			for(int j=0;j<y;j++){
				r=ran.nextInt(2);//產生亂數
				z[i][j]=r;//將亂數儲存在陣列
				System.out.print(r+" ");//輸出亂數
				if(z[i][j]==1&&j!=0){
					if(z[i][j-1]==1)
						n++;//計算橫向連續1的數量
				}
			}
			System.out.println();//換行
		}
		for(int i=0;i<x;i++){
			for(int j=0;j<y;j++){
				if(z[j][i]==1&&i!=0){
					if(z[j][i-1]==1)
						n++;//計算直向連續1的數量
				}
			}	
		}
		System.out.print("total: "+n);//輸出結果
	}
}
