package ce1002.e2.s102502039;

import java.util.Scanner;

public class E2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Please input first number (3~10): ");// 輸出題目
		int l = input.nextInt();
		while (l < 3 || l > 10) {
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		int w = input.nextInt();
		while (w < 3 || w > 10) {
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();
		}
		int[][] array = new int[l][w];// 產生亂數
		for (int row = 0; row < l; row++) {
			for (int column = 0; column < array[row].length; column++) {
				array[row][column] = (int) (Math.random() * 2);
			}
		}
		for (int row = 0; row < l; row++) {
			for (int column = 0; column < w; column++) {
				System.out.print(array[row][column] + " ");
			}
			System.out.println();
		}
		int a = 0;// 計算個數
		for (int row = 0; row < l; row++) {
			for (int column = 0; column < (w - 1); column++) {
				if (array[row][column] == 1) {
					if (array[row][column] == array[row][(column + 1)])
						a++;
				}
			}
		}
		int b = 0;
		for (int column = 0; column < w; column++) {
			for (int row = 0; row < (l - 1); row++) {
				if (array[row][column] == 1) {
					if (array[row][column] == array[(row + 1)][column])
						b++;
				}
			}
		}
		int total = a + b;
		System.out.println("total: " + total);

	}
}
