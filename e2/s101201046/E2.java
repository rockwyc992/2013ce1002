package ce1002.e2.s101201046;

import java.util.Scanner;
import java.lang.Math;

public class E2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int i,j,w,l,count=0;//w width,l length
		int[][] map;
		do{
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();
			if(l<3 || l>10)
				System.out.println("Out of range!");
		}while(l<3 || l>10);//input l
		do{
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();
			if(w<3 || w>10)
				System.out.println("Out of range!");
		}while(w<3 || w>10);//input w
		map = new int[l][w];//construct l*w map
		for(i=0;i<l;i++){
			for(j=0;j<w;j++){
				map[i][j] = (int)(Math.random()*2);//randomly generate 0 or 1
				if(map[i][j] == 1){
					if(i!=0 && map[i-1][j] == 1)//count continuous 1s
						count++;
					if(j!=0 && map[i][j-1] == 1)//count continuous 1s
						count++;
				}
				System.out.print(map[i][j]+" ");
			}
			System.out.println();
		}//output map
		System.out.println("total: "+count);//output total number of continuous 1s
	}
	
}