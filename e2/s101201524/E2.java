package ce1002.e2.s101201524;
import java.util.Scanner;

public class E2 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int l = 0, w = 0, j = 0, k = 0, count = 0;
		//create a matrix to memorize the output
		int[][] matrix;
		matrix = new int[11][11];
		for(j = 0; j < 11; j++){
			for(k = 0; k < 11 ;k++)
				matrix[j][k] = 0;
		}
		//asking for I
		while(l < 3 || l > 10){
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();
			if(l < 3 || l > 10)
				System.out.println("Out of range!");
		}
		//asking for W
		while(w < 3 || w > 10){
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();
			if(w < 3 || w > 10)
				System.out.println("Out of range!");
		}
		//output the matrix and count number of groups at same time
		for(j = 1; j <= l; j++){
			for(k = 1; k <= w ;k++){
				matrix[j][k] = ((int)(Math.random()*2));
				System.out.print(matrix[j][k] + " ");
				if(matrix[j][k] == 1){
					if(matrix[j-1][k] == 1)
						count++;
					if(matrix[j][k-1] == 1)
						count++;
				}
			}
			System.out.println("");
		}
		//output the number of the groups
		System.out.print("total: " + count);
	}
}
