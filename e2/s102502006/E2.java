package ce1002.e2.s102502006;

import java.util.Scanner; // 輸出
import java.util.Random; // 變數

public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		Random ran = new Random();

		int [][] square = new int [20][20]; // 方框
		
		int l =0; // 第一個數字
		int w =0; // 第二個數字
		int counter =0; // 計數
		
		while(l<3 || l>10){ // 判斷輸入
			System.out.print("Please input first number (3~10): ");
			l = scanner.nextInt();
			if(l<3 || l>10)System.out.println("Out of range!");
		}
		
		while(w<3 || w>10){
			System.out.print("Please input second number (3~10): ");
			w = scanner.nextInt();
			if(w<3 || w>10)System.out.println("Out of range!");
		}
		
		
		for(int i=1;i<=l;i++){
			for(int j=1;j<=w;j++){
				square[i][j] = (ran.nextInt(2));
				// System.out.printf("%d ",square[i][j]);
				if(square[i][j]==1){ // 判斷四周
					if(square[i-1][j]==1)counter++;
					if(square[i][j-1]==1)counter++;
					if(square[i+1][j]==1)counter++;
					if(square[i][j+1]==1)counter++;
				}		
			}
		}	
		
		for(int i=1;i<=l;i++){ // 輸出
			for(int j=1;j<=w;j++){
				System.out.print(square[i][j] + " ");
			}
			System.out.println();
		}
		
		System.out.printf("total: %d",counter);
	}
}
