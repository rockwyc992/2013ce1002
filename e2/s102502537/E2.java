package ce1002.e2.s102502537;

import java.util.Scanner;//scanner is in the java.util package

public class E2 {
	public static void main (String [] args){
		
		Scanner input = new Scanner (System.in);//creat a scanner object
		
		System.out.print("Please input first number (3~10): ");//prompt the user to enter a number
		int number1 = input.nextInt();
		while (number1 < 3 || number1 >10){
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			number1 = input.nextInt();
		}//if the number out of range , the user can enter again
		System.out.print("Please input second number (3~10): ");//prompt the user to enter a number
		int number2 = input.nextInt();
		while (number2 < 3 || number2 >10){
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			number2 = input.nextInt();
		}//if the number out of range , the user can enter again
		
		int [][] array;
		array = new int [number1][number2];//宣告陣列
		
		for ( int i = 0 ; i < number1 ; i++){
			for ( int j = 0 ; j < number2 ; j++){
				array [i][j] = (int)(Math.random()*2);
			}
		}//放入亂數
		for ( int i = 0 ; i < number1 ; i++){
			for ( int j = 0 ; j < number2 ; j++){
				System.out.print(array [i][j]);
				System.out.print(" ");
			}
			System.out.print("\n");
		}//輸出亂數
		int total = 0;
		for ( int i = 0 ; i < number1  ; i++){
			for ( int j = 0 ; j < number2 - 1 ; j++){
				if ( array[i][j] == 1 && array[i][j] == array [i][j+1]){
					total++;
				}			
			}
		}
		for ( int i = 0 ; i < number1 - 1 ; i++){
			for ( int j = 0 ; j < number2 ; j++){
				if ( array[i][j] == 1 && array[i][j] == array [i+1][j]){
					total++;
				}			
			}
		}//計算相鄰皆為1的次數
		System.out.print("total: ");
		System.out.print(total);//輸出
	}

}