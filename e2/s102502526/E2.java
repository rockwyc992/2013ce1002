package ce1002.e2.s102502526;
import java.util.Scanner;
import java.util.Random;

public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

				Scanner scanner = new Scanner(System.in);
				Random ran = new Random();						//亂數
				int a;                                          //列
				int b;                                          //行
				int sum = 0;
				do {
					System.out.print("Please input first number (3~10): ");				//超除則重新輸入
					a = scanner.nextInt();
					if (a > 10 || a < 3) {
						System.out.println("Out of range!");
					}
				} while (a > 10 || a < 3);
				do {
					System.out.print("Please input second number (3~10): ");			//超除則重新輸入
					b = scanner.nextInt();
					if (b > 10 || b < 3) {
						System.out.println("Out of range!");
					}
				} while (b > 10 || b < 3);
				int[][] arr = new int[a][b];					//陣列
				for (int i = 0; i < a; i++) {												//放亂數
					for (int j = 0; j < b; j++)
						arr[i][j] = ran.nextInt(2);
				}
				for (int i = 0; i < a; i++) {
					for (int j = 0; j < b; j++) {
						System.out.print(arr[i][j] + " ");
					}
					System.out.println("");
				}
				for (int i = 0; i < a-1; i++) {												//算是否重複
					for (int j = 0; j < b-1; j++) {
						if (arr[i][j] == 1 && arr[i][j + 1] == 1) {
							sum++;
						}
						if (arr[i][j] == 1 && arr[i + 1][j] == 1) {
							sum++;
						}
					}
				}
				System.out.print("total: " + sum);
				scanner.close();
			}
		}
