package ce1002.e2.s995002046;


import java.util.Random;
import java.util.Scanner;


class E2{
	
	public static void main(String[] args) { 
		int firstnum=0;
		int secondnum=0;
		Scanner scanner;
		Random random= new Random();
		System.out.print("Please input the first number (3~10): ");
		while(5>3){//loop until input a right number
			scanner = new Scanner(System.in);
			firstnum=scanner.nextInt();
			if(3 <= firstnum && firstnum <= 10){
				break;
			}
			else
				System.out.print("Out of range ! Please input again(3~10): ");
			
		}
		System.out.print("Please input the second number (3~10): ");
		while(5>3){//loop until input a right number
			scanner = new Scanner(System.in);
			secondnum=scanner.nextInt();
			if(3 <= secondnum && secondnum <= 10){

				break;
			}
			else
				System.out.print("Out of range ! Please input again(3~10): ");
			
		}
		scanner.close();
		int arr[][]=new int[firstnum][secondnum];
		for(int i=0; i<firstnum; i++){//給陣列的每個元素亂數值0~1 並輸出陣列
			for(int j=0; j<secondnum; j++){
				arr[i][j]=random.nextInt(2);
				System.out.print(arr[i][j]+" ");
			}
			System.out.print("\n");
		}
		int total=0;
		for(int i=0; i<firstnum; i++){//計算total 並輸出
			for(int j=0; j<secondnum; j++){
				if(arr[i][j]==1 && i!=firstnum-1){
					if(arr[i+1][j]==1)
						total++;
				}
				if(arr[i][j]==1 && j!=secondnum-1){
					if(arr[i][j+1]==1)
						total++;
				}
			}
		}
		System.out.print("total: "+total);
	}
}