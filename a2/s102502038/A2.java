package ce1002.a2.s102502038;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class A2 {
	private static int c(int num,int take){
		//choice function
		take = take>(num-take)?take:(num-take);
		int count = 1;
		if(take == num) return 1;
		for(int i = take+1;i<=num;i++){
			count *= i;
		}
		for(int i = num - take;i>0;i--){
			count /= i;
		}
		return count;
	}
	public static void main(String[] args){
		String inputln = null;
		int num = 0;
		// input section
		while(true){
			System.out.println("Please input a number (1~10): ");
			try{
				BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
				inputln = buff.readLine();
				num = Integer.parseInt(inputln);
			}catch(IOException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}catch(java.lang.NumberFormatException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}
			if(num > 10 || num < 1){
				System.out.println("Out of range!");
				continue;
			}
			break;
		}
		// end of input section
		int[][] arr = new int[num][(int) Math.pow(2, num)];		//I love the way without array.
		for(int i = 0;i<num;i++){
			for(int j = 0;j<=i;j++){
				System.out.print(String.valueOf(c(i,j)) + " ");
			}
			System.out.println("");
		}
	}

}
