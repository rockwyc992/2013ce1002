package ce1002.a2.s102502007;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int layer;//the layer of the triangle
		do
		{
		System.out.println("Please input a number (1~10): ");
		layer = input.nextInt();
		if(layer>10 || layer<1)
			System.out.println("Out of range!");//loop till the correct number is input
		}while(layer>10 || layer<1 );
		input.close();
		int[][] array;
		array= new int [10][10];//the triangle is 10 layers atmost
		for(int i=0;i<layer;i++)
		{//column
			for(int j=0;j<=i;j++)
			{//row
				if(j==0 || j==i)
				array[i][j]=1;//set the first and the last number as "1"
				else
				{
					array[i][j]=array[i-1][j-1]+array[i-1][j];
				}//the others are the result of the former layer's two numbers' addition
			}
		}
		for(int i=0;i<layer;i++)
		{
			for(int j=0;j<=i;j++)
			{
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}//print the triangle
		
	}

}
