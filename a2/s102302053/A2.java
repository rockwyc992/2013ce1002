package ce1002.a2.s102302053;

import java.util.Scanner;

public class A2 {
	
	private static Scanner scanner;

	public static void main(String[] args) {		
		int num;
		scanner = new Scanner(System.in);
		System.out.println("Please input a number (1~10): ");//指示使用者書輸入範圍內的數字來做為層數,如果超出範圍,提示使用者重新輸入.
		do{
			num = scanner.nextInt();
			if(num<1 || num >10){
				System.out.println("Out of range!\nPlease input a number (1~10): ");
			}
			
		}while (num<1 || num >10);	
		int [][] pascal = new int [10][10];	//宣告2維陣列
		pascal[0][0] = 1;//先指定好最上面兩層的項
		pascal[1][0] = 1;
		pascal[1][1] = 1;
		for (int i = 2; i < num; i++){//從第2行最左邊,由左而右,開始存入三角形每個項的數字
			for(int j = 0; j <= i; j++){
				if (j == 0){
					pascal[i][0] = 1;//每層第一項都是1
				}
				else if (j == i){
					pascal[i][j] = 1;//每層最後一項都是1
				}
				else{
					pascal[i][j]=pascal[i-1][j]+pascal[i-1][j-1];//每一項為上一層同位置的數字跟左上角的兩數字和
				}	
			}
		}
		
		for(int i = 0; i < num; i++){//由第一層開始由左而右,由上而下輸出巴斯卡三角形
			for(int j = 0; j <= i;j++ ){
				System.out.print(pascal[i][j] + " ");
			}
			System.out.println();
		}

	}

}
