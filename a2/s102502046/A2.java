package ce1002.a2.s102502046;
import java.util.Scanner;

public class A2 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		int a; //宣告一個整數
		int[][] arr = new int[10][10] ;		//宣告一個二微陣列
		
		for(int i=0 ; i<10 ; i++)
		{
			arr[i][0]=1;
			arr[i][i]=1;
		}
		
		for(int i=2 ; i<=9 ; i++)	//計算數值
			for(int j=1 ; j<i ; j++)
				arr[i][j]=arr[i-1][j]+arr[i-1][j-1];
		System.out.println("Please input a number (1~10): "); //輸出一串字
		Scanner in = new Scanner (System.in); //輸入
		a = in.nextInt(); 
		while(a<1||a>10)		//判斷是否在範圍內
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			a = in.nextInt(); 
		}
		for (int i=0 ; i<a ; i++) //印出結果
		{
			for (int j=0 ; j<i+1 ; j++)
				System.out.print(arr[i][j]+" ");
			System.out.println("");
		}
	}
}
