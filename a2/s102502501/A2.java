package ce1002.a2.s102502501;
import java.util.Scanner;	//#include<stdio.h>的宣告
public class A2{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int number = 10;
		System.out.println("Please input a number (1~10): ");
		number = input.nextInt();
		while ( number < 1 || number > 10)	//迴圈
		{
			System.out.println("Out of range!");
			
			System.out.println("Please input a number (1~10): ");
			number = input.nextInt();
		}
		int[][] x= new int [number+1][number+1];     //宣告二維陣列
		for(int i = 0; i < number; i++)
		{
			for(int j = 0;j <= i; j++)
			{
				if(i==0 || i==j)         //對角線和第一航都是1
						x[i][j]=1;        
					
				else
					x[i][j] = x[i-1][j-1] + x[i-1][j];    //其餘由上面兩者相加
			}
		}
		for(int a = 0;a < number; a++)
		{
			for(int b = 0; b < number;b ++)
			{
				if(x[a][b]!=0)
				      System.out.print(x[a][b]+" ");
			}
			System.out.println("");
		}
		
	}

}
