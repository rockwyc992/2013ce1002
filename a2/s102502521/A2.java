package ce1002.a2.s102502521;
import java.util.*;

public class A2 {
	private static Scanner scanner;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		scanner=new Scanner(System.in);
		
		//設定變數
		int innum;
		int [][]arr;
		arr=new int[9][9];
		
		//判斷值是否合理
		do{
			System.out.println("Please input a number (1~10): ");
			innum=scanner.nextInt();
    	
			if(innum>10||innum<1){
				System.out.println("Out of range!");    
			}    	
		} while (innum>10||innum<1);
		
		//存入巴斯卡三角形
		for(int y=0,count=1;y<innum;y++){
			for(int x=0;x<count;x++){
				if(x==0||x==count-1){
					arr[x][y]=1;
				}
				else{
					arr[x][y]=arr[x-1][y-1]+arr[x][y-1];
				}
			}
			count++;
		}
		
		//印出三角形
		for(int y=0,count=1;y<innum;y++){
			for(int x=0;x<count;x++){
				System.out.print(arr[x][y]+" ");
			}
			System.out.println();
			
			count++;
		}
	}
}
