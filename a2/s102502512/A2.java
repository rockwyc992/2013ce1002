package ce1002.a2.s102502512;
import java.util.*;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int[][] btr= new int[10][10];
		int n=0;
		int cont=1;
		do{													/* Line 12~19: to let user input the number. */
		System.out.println("Please input number (1~10): ");
		n=input.nextInt();
		if(n>10||n<1)
		{
			System.out.println("Out of range!");
		}
		}while(n>10||n<1);
		for(int i=0;i<n;i++)								/* Line 20~35: to caculate and print the pascal triangle. */
		{
			for(int j=0;j<cont;j++)
			{
				if(j==0||i==j)
				{
					btr[i][j]=1;
				}
				if(i>1&&j!=0)
				{
					btr[i][j]=btr[i-1][j-1]+btr[i-1][j];
				}
				System.out.print(btr[i][j]+" ");
			}
			cont++;
			System.out.println();
		}
	}

}
