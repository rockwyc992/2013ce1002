package ce1002.a2.s102502541;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int number;
		int triangle[][]= new int[11][11];
		Scanner scanner = new Scanner(System.in);
		do
		{
			System.out.println("Please input a number (1~10): ");
			number = scanner.nextInt();
			if(number<1 || number>10)
				System.out.println("Out of range!");
		}while(number<1 || number>10);
	for(int i=1; i<=number; i++)
	{
		for(int j=1; j<=i; j++)
		{
			if(j==1 || j==i)
				triangle[i][j]=1;//頭尾項皆為1
			else 
				triangle[i][j]=triangle[i-1][j-1]+triangle[i-1][j];//上行兩項相加=下行一項
		}
	}
	for(int i=1; i<=number; i++)
	{
		for(int j=1; j<=i; j++)
		{
			if(j==i)
				System.out.println(triangle[i][j]);
			else 
				System.out.print(triangle[i][j]+" ");
		}
	}
	}

};
