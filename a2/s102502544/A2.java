package ce1002.a2.s102502544;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please input a number (1~10): ");//輸出問題
		int num = input.nextInt();//輸入數字
		while(num<1 || num>10){//設定範圍
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			num = input.nextInt();
		}
		int arr[][];//設定陣列
		arr = new int[num][num];//陣列大小
		int n=1;
		for(int i=0 ; i<num ; i++){//記錄巴斯卡三角形到陣列中
			for(int j=0 ; j<n && n<num+1 ; j++){
				arr[i][0]=1;
				arr[i][n-1]=1;
				if(i>1 && (j>0 && j<n-1) )
				arr[i][j]=arr[i-1][j]+arr[i-1][j-1];
			}
			n++;
		}
		n=1;
		for(int y=0 ; y<num ; y++){//輸出巴斯卡三角形的陣列
			for(int x=0 ; x<n && n<num+1 ; x++){
				System.out.print(arr[y][x]+" ");
			}
			System.out.println();
			n++;
		}

	}

}
