package ce1002.a2.s102502542;
import java.util.Scanner;
public class A2 {
	private static Scanner scanner;
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please input a number (1~10): "+"\n");//輸出
		int c = scanner.nextInt();//輸入
		while(c<1||c>10)//當c<1或c>10進入迴圈
		{System.out.print("Out of range!");
		System.out.print("\n");
		System.out.print("Please input a number (1~10): "+"\n");
		c = scanner.nextInt();
		}
		int arr[][];//宣告陣列
		arr = new int[c][c];//宣告陣列大小
		arr[0][0]=1;
		for(int a=1;a<c;a++)
		{
		arr[0][a]=0;
		}
		for(int a=1;a<c;a++)
		{
			arr[a][0]=1;
			for(int b=0;b<c-1;b++)
		{
			arr[a][b+1]=arr[a-1][b+1]+arr[a-1][b];//巴斯卡三角的打法
		}
		}
		for(int a=0;a<c;a++)//迴圈  輸出最後結果
		{for(int b=0;b<c;b++)
		{
			if(arr[a][b]==0)
				b++;
			else if(arr[a][b]!=0)
				System.out.print(arr[a][b]+" ");
		}
		System.out.print("\n");
		}
		// TODO Auto-generated method stub

	}

}
