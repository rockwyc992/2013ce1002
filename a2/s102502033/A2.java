package ce1002.a2.s102502033;
import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input= new Scanner(System.in);
		int N=0;//幾階層
		System.out.println("Please input a number (1~10): ");
		N= input.nextInt();
		while(N<1 || N>10)//出錯迴圈
		{
			System.out.println("Out of range!\nPlease input a number (1~10): ");
			N= input.nextInt();
		}
		
		int [][]arr= new int[N][N] ;
		for(int i=0;i<N;i++)//從第一列開始存
		{
			for(int j=0;j<=i;j++)
			{
				if(j==0 || j==i)
					arr[j][i]=1;//列的前端和後端皆為1
				else
					arr[j][i]=arr[j-1][i-1]+arr[j][i-1];//他的左上和正上方的數字加起來
				System.out.print(arr[j][i]+" ");
			}
			System.out.println();
		}	

	}

}
