package ce1002.a2.s102502034;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		int userin;         //宣告變數 陣列
		int k = 0;
		int[][] Pascal = new int[10][10];
		for (int j = 0; j <= 9; j++) {    //初始化陣列
			for (int i = 0; i <= 9; i++) {
				Pascal[j][i] = 1;
			}
		}
		Scanner scanner = new Scanner(System.in);
		//建立INPUT此OBJECT
		System.out.print("Please input a number (1~10):");
		userin = scanner.nextInt();
		//將userin 設定為 nextint
		while (userin <= 1 || userin > 10) {       
			System.out.println("Out of range!");
			System.out.print("Please input a number (1~10):");
			userin = scanner.nextInt();
		}
		//確定輸入數字範圍正確
		if (userin > 2) {
			for (int x = 2; x < userin; x++) {
				for (int y = 1; y <= x - 1; y++) {
					Pascal[x][y] = Pascal[x - 1][y - 1] + Pascal[x - 1][y];

				}
			}
		}
		//做一個巴斯卡三角形
		for (int j = 0; j <= userin - 1; j++) {

			for (int i = 0; i <= j; i++) {
				System.out.print(Pascal[j][i]);
				System.out.print(" ");
			}
			if(j==userin-1)
			break;
			System.out.println();

		}
		//輸出結果

	}
}
