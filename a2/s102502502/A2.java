package ce1002.a2.s102502502;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int l,n,i;
		final int f = 10;                                          //設定數值最大為10
		int [] [] triangle = new int [f] [f];                      //宣告陣列
		System.out.println("Please input a number (1~10): ");      //請求輸入數字
		n = input.nextInt();                                         
		while (n < 1 || n > 10)                                    //當數字超出範圍，則顯示超出範圍並要求再次輸入
		{
			System.out.println("Out of range! ");
			System.out.println("Please input a number (1~10): ");
			n = input.nextInt();
		}	
		triangle[0][0] = 1;                                        //巴斯卡三角形第一數為1
		System.out.println(triangle [0][0] + " ");
		for (l = 1; l < n; l++)                                    //從第2層開始
		{
			for (i =0; i <= l; i++)
			{
				if (i == 0)                                        //每列第一數為1
				{
					triangle [l] [i] = 1;
					System.out.print(triangle [l][i] + " ");
				}
				else if (i == l)                                   //每列最後一數為1，並換行
				{
                    triangle [l] [i] = 1;                        
					System.out.println(triangle [l][i] +" ");
				}
				else                                              //依巴斯卡三角形規則執行
				{
                    triangle [l] [i] = triangle [l-1] [i] +  triangle [l-1] [i-1];
					System.out.print(triangle [l][i] + " ");	
				}	
			}
		}	
	}
}