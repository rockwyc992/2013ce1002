package ce1002.a2.s102502050;

import java.util.Scanner;

public class A2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int [][]pascal;
		int input;
		do
		{
			//輸入並檢查
			System.out.println("Please input a number (1~10):");
			input = scanner.nextInt();
			if( input<=0 || input>10)
			{
				System.out.println("Out of range!");
			}
			
		}while(input<=0 || input>10);
		
		
		pascal = new int [input+1][input+1];
		for(int i=0;i<input;i++)
		{
			pascal[i][0]=1;//給初始值
			System.out.print("1");
			for(int j=1;j<=i;j++)
			{
				pascal[i][j]=pascal[i-1][j-1]+pascal[i-1][j];//計算巴斯卡三角形的值
				System.out.print(" "+pascal[i][j]);
			}
			pascal[i][i+1]=0;//給初始值
			System.out.println("");
		}
		
		
	}

}
