package ce1002.a2.s102502049;

import java.util.Scanner;

public class A2 {

	public static void ShowPascal(int[][] array, int index, int size) {
		for (int i = 0; i < size; i++) {
			System.out.print(array[index][i] + " "); // print answer
		}
		System.out.print("\n"); // next line
	}

	public static void main(String[] args) {
		java.util.Scanner input = new Scanner(System.in);
		int[][] pascal = new int[2][10];
		pascal[0][0] = 1; // initialize two array. One for 1,the other for 1 1
		pascal[1][0] = 1;
		pascal[1][1] = 1;
		int number = 1; // range

		do {
			if (number > 10 || number < 1) {
				System.out.println("Out of range!");
			}
			System.out.println("Please input a number (1~10): ");
			number = input.nextInt();
		} while (number > 10 || number < 1); // ask to input right range

		if (number == 1) // output directly when number==1or2
		{
			System.out.println("1");
		}

		else if (number == 2) {
			System.out.println("1\n1 1");
		}

		else // do pascal
		{
			System.out.print("1\n1 1\n");
			int size = 2;
			for (int j = 0; j < (number - 2); j++) {
				if ((j % 2) == 0) // add alternately
				{
					for (int i = 1; i < size; i++) {
						pascal[0][i] = pascal[1][i] + pascal[1][i - 1];
					}
					pascal[0][size] = 1;
					size++;
					ShowPascal(pascal, 0, size);
				}

				if ((j % 2) == 1) {
					for (int i = 1; i < size; i++) {
						pascal[1][i] = pascal[0][i] + pascal[0][i - 1];
					}
					pascal[1][size] = 1;
					size++;
					ShowPascal(pascal, 1, size);
				}
			}
		}

	}

}