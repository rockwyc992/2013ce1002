package ce1002.a2.s102502026;
import java.util.Scanner;
public class A2 {
	private static Scanner input;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		input = new Scanner(System.in);
		int number = 0;		
		while(number<1 || number>10)		
		{
			System.out.print("Please input a number (1~10): \n");		
			number = input.nextInt();		
			if(number<1 || number>10)		
			{
				System.out.print("Out of range!\n");
			}
		}
        int[][] pt = new int[11][11];
        pt[0][0] = 1;

        for (int x = 1; x<number+1;x++) 
        {
            for (int y = 1; y<number+1; y++) 
            {
                pt[x][y] = pt[x-1][y-1] + pt[x-1][y];
                if (pt[x][y]>0) 
                {
                    System.out.print(pt[x][y] + " ");
                }
            }
            System.out.println();
        }        
    }
}
	


