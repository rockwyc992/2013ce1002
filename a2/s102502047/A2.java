package ce1002.a2.s102502047;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		System.out.println("Please input a number (1~10):");
		Scanner p = new Scanner(System.in);
		int n=p.nextInt();
		for(;n>10||n<1;)
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10):");
			n=p.nextInt();
		}
		
		int t[][] = new int[n][n];//宣告二微陣列
		for(int i=0;i<n;i++)//頭尾都是1
		{
			t[i][0]=1;
			t[i][i]=1;
		}
		for(int i=2;i<n;i++)//數字推算
			for(int j=1;j<i;j++)
			{
				t[i][j]=t[i-1][j-1]+t[i-1][j];
			}
		for(int i=0;i<n;i++)//輸出巴斯卡陣列
		{
			for(int j=0;j<=i;j++)
				System.out.print(t[i][j]+" ");
			System.out.print("\n");
		}
	}

}
