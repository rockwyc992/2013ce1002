package ce1002.a2.s100502022;

import java.util.Scanner;

public class A2 {
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		int number;
		boolean option =false;
		System.out.println("Please input a number(1~10):");
		number = s.nextInt();
		//rule of TA asked 1~10
		do{
			if(number<1||number>10){
				//error 
				System.out.println("Out of range!\nPlease input a number(1~10):");
				number = s.nextInt();
			}
			else{
				//initial
				int[] arrplus = new int[number];
				arrplus[0]=1;
				//for loop about answer of every line
				for(int i=1;i<=number;i++){
					//into method
					arrplus = swapandAdd(arrplus,i);
					//each value in a line
					for(int j=0;j<i;j++){
						//output the answer
						System.out.print(arrplus[j]+" ");
					}
					System.out.println();
				}
				option=true;
			}
		}while(option==false);
		
	}
	//make a new array store the array[0]=0 and put the initial array behind the 0
	public static int[] swapandAdd( int[] arr, int times){
		int[] temp= new int[times+1];
		for(int i=times-1;i>0;i--){
			temp[i]=arr[i-1];
		}
		temp[0]=0;
		for(int i=0;i<times;i++){
			temp[i]=temp[i]+arr[i];
		}
		return temp;
	}
}
