package ce1002.a2.s102502030;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	int num;
	//存放陣列大小用
    Scanner input = new Scanner(System.in);
    do
    {
        System.out.println("Please input a number (1~10): ");
        num = input.nextInt();
        if( num<1 || num>10 )
        {
        	System.out.println("Out of range!");
        }
    } while( num<1 || num>10);
    //輸入陣列大小
    input.close();
    
    int [][] data = new int [num][num];
    for( int i=0; i<num; i++ )
    {
    	data[i][0]=1;
    }
    //將第一行設為1
    for( int i=1; i<num; i++ )
    {
    	data[0][i]=0;
    }
    //將第一列其餘設為0
    for( int i=1; i<num; i++ )
    {
    	for( int j=1; j<num; j++ )
    	{
    		data[i][j]=data[i-1][j-1]+data[i-1][j];
    	}
    }
    //計算巴斯塔三角形各項數值
    for( int i=0; i<num; i++ )
    {
    	for( int j=0; j<num; j++)
    	{
    		if( data[i][j]!=0 )
    			System.out.print(data[i][j] + " ");
    	}
    	System.out.print("\n");
    }
    //輸出非0項
	}

}
