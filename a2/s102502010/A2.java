package ce1002.a2.s102502010;
import java.util.Scanner;
public class A2 {
	public static void main(String[] args) {
		int n,i,j;
		final int m=10;
		int pa[][] = new int[m][m];
		Scanner input = new Scanner(System.in);
		System.out.println("Please input a number (1~10):");
		n=input.nextInt();
		while(n<1 || n>10)
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10):");
			n=input.nextInt();
		}
		for(i=0;i<n;i++)  //歸零
			for(j=0;j<n;j++)
				pa[i][j]=0;
		pa[0][0]=1;
		if(n>1)
		{
			for(j=1;j<n;j++)  //跑巴斯卡三角形
		    {
			   for(i=0;i<n;i++)
			   {
				  if(i==0 || (pa[i][j-1]==0 && pa[i-1][j-1]!=0))
					 pa[i][j]=1;
				  else
					  pa[i][j]=pa[i-1][j-1]+pa[i][j-1];
			   }
		    }	
		}
		for(j=0;j<n;j++)
		{
			for(i=0;i<n;i++)
				if(pa[i][j]!=0)
					System.out.print(pa[i][j] + " ");  //輸出
			System.out.println("");
		}
	}
}
