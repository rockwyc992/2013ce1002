package ce1002.a2.s102502016;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		int number;
		int[][] array = new int[10][10];
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please input a number (1~10):");
		number = scanner.nextInt();
		while (number < 1 || number > 10) {
			System.out.println("Out of range !");
			System.out.print("Please input a number (1~10):");
			number = scanner.nextInt();
		}
		for (int i = 0; i < 10; i++) {//算出巴斯卡三角形的值
			array[i][0] = 1;

			for (int j = 1; j < 10; j++) {
				if (i != 0 && j <= i) {
					if (i == j)
						array[i][j] = 1;
					else
						array[i][j] = array[i - 1][j - 1] + array[i - 1][j];
				}
			}
		}
		for (int i = 0; i < number; i++) {//列出巴斯卡三角形的值
			for (int j = 0; j <= i; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
	}

}
