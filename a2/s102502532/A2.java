package ce1002.a2.s102502532;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Please input a number (1~10): ");
		int number = input.nextInt();
		while (number < 1 || number > 10) {
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			number = input.nextInt();
		}

		int[][] triangle = new int[10][10]; // 宣告建立int陣列
		int k = 1; // 第n列的數字個數為n個

		while (k <= number) { // 判斷 每列個數
			for (int y = 0; y < number; y++) {
				for (int x = 0; x < k; x++) {
					if (x == 0) { // 判斷
						triangle[y][x] = 1;
					} else if (x == k - 1) {
						triangle[y][x] = 1;
					} else {
						triangle[y][x] = triangle[y - 1][x]
								+ triangle[y - 1][x - 1];
					}
					System.out.print(triangle[y][x] + " "); // 印出
				}
				k++;
				System.out.println();
			}
		}

	}
}
