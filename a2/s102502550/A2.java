package ce1002.a2.s102502550;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		Scanner in = new  Scanner(System.in);                               //宣告輸入物件
		int a = 0;
		
		
		do{                                                                 //輸入提示
			
		   System.out.println("Please input a number (1~10): ");	
           a = in.nextInt();
           
           if(a<1 || a>10){
        	   System.out.println("Out of range!");
           }
		}while(a<1 || a>10);
		
		int[][] arr = new int[15][15];                                      //宣告陣列
		
		for(int i=1;i<=a;i++){                                              //巴斯卡三角形
			for(int j=1;j<=i;j++){
				if(j==1 || j==i){
					arr[i][j]=1;	
				}
				else{
					arr[i][j] = arr[i-1][j-1] + arr[i-1][j];
				}
			}
			
		}

		for(int i=1;i<=a;i++){                                              //輸出結果
			for(int j=1;j<=i;j++){
				System.out.print(arr[i][j] + " ");
			}
			System.out.println("");
		}
	}

}
