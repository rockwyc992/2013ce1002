package ce1002.a2.s102502018;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		int num=0;
		Scanner scan = new Scanner (System.in);		
		do
		{
			System.out.println("Please input a number (1~10): ");
			num = scan.nextInt();
			if(num<0||num>10)
			{
				num = scan.nextInt();
				System.out.println("Out of range!");
				System.out.println("Please input a number (1~10): ");				
			}
		}while(num<0||num>10);                      //判斷範圍是否正確
		int arr[][];
		arr= new int [num][num];
		arr[0][0]=1;
		for(int a=0;a<num;a++)
		{			
			for(int b=0;b<=a;b++)
			{
				if(b==0)                            //每列第一個數字為1
				{
					arr[a][b]=1;
				}
				else if(b==1)              
				{
					arr[a][b]=arr[a-1][0]+arr[a-1][b];
				}
				else
				{
					arr[a][b]=arr[a-1][b-1]+arr[a-1][b];
				}
				System.out.print(arr[a][b]+" ");
			}			
			System.out.println("");
		}

	}

}
