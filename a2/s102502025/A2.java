package ce1002.a2.s102502025;
import java.util.Scanner;		//給予輸入指令
public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);		//啟動指令
		
		int n = 0;		//創輸入值
		
		while(n<1 || n>10)		//輸入不符合將迴圈
		{
			System.out.println("Please input a number (1~10): ");
			n = input.nextInt();
			if(n<1 || n>10)
			{
				System.out.println("Out of range!");
			}
		}
		
		int nn[][] = new int[11][11];		//創巴斯卡三角形矩陣
		nn[0][0]=1;
		
		for(int x=1 ; x<n+1 ; x++)		//算出巴斯卡三角形
		{
			for(int y=1 ; y<n+1 ; y++)
			{
				nn[x][y] = nn[x-1][y-1] + nn[x-1][y];
                if (nn[x][y]!=0) 
                {
                    System.out.print(nn[x][y] + " ");
                }
            
			}
			System.out.println();
		}
		
		input.close();

	}

}
