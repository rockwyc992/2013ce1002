package ce1002.a2.s102502002;
import java.util.Scanner;

public class A2 {

	private static Scanner scanner;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		scanner = new Scanner(System.in);
		System.out.println("Please input a number (1~10): ");
		int n=0;
		int [][] arr;
		arr = new int[11][11]; //設一11*11陣列
		n = scanner.nextInt();
		while(n<1||n>10)  //判斷是否超出範圍
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			n = scanner.nextInt();
		}
		n=n+1;

		for(int i=0;i<10;i++) //將陣列所有元素初始化為0
		{
			for(int j=0;j<10;j++)
			{
				arr[i][j]=0;
			}
		}

		arr[1][1]=1;//第一項設為1
		for(int i=2;i<n;i++)
		{
			for(int j=1;j<n;j++)
			{
				arr[i][j]=arr[i-1][j]+arr[i-1][j-1]; //帕斯卡定理
			}
		}

		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n;j++)
			{
				if(arr[i][j]!=0)
					System.out.print(arr[i][j]); //輸出不等於0的arr值
					System.out.print(" ");
			}
			System.out.println(); //換行
		}

		scanner.close(); //關閉Scanner
	}

}
