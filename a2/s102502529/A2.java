package ce1002.a2.s102502529;

import java.util.Scanner;

public class A2 {
	public static void main(String[] args) {
		Scanner s= new Scanner(System.in);		
		int [][]x ;																
		x=new int [11][10];															//宣告 array
		do{																			//判斷書入值
			System.out.println("Please input a number (1~10): ");
			x[0][0]=s.nextInt();
			if(x[0][0]<1||x[0][0]>10)
				System.out.println("Out of range!");
		}while(x[0][0]<1||x[0][0]>10);
		x[1][0]=1;																	//第一行 第一數
		for(int i=2;i<=x[0][0];i++)
		{
			x[i][0]=1;																//每行首末都為0
			x[i][i-1]=1;
			for(int g=1;g<i-1;g++)
			{
				x[i][g]=x[i-1][g]+x[i-1][g-1];										//上一行原位 跟上一行原位的前一位
			}
		}
		for(int i=1;i<=x[0][0];i++)													//輸出
		{
			for(int g=0;g<i;g++)
			{
				System.out.print(x[i][g]);
				System.out.print(" ");
			}
			if(i==x[0][0])															//為了最後一行不要有空白
				break;
			System.out.println();
		}
	}

}
