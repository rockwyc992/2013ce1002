package ce1002.a2.s102502553;

import java.util.Scanner;//建立輸入系統

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);//輸入系統
		int num;
		do
		{
		System.out.println("Please input a number (1~10):");//輸出字串
		num = input.nextInt();//輸入變數
		if(num > 10 || num < 1)
	    System.out.println("Out of range!");
		}
		while(num > 10 || num < 1);
		
		int [][] arr = new int[num][num];//建立陣列
		for(int i = 0;i < num;i++)
		{
			for(int j = 0;j <= i; j++)
				{
				if(j == 0 || j == i)
				{
					arr[i][j] = 1;
				}
				else
				{
					arr[i][j]=arr[i-1][j-1] + arr[i-1][j];
				}
				}
		}
		
		for(int i = 0;i < num;i++)
		{
		for(int j = 0;j <= i ;j++)
			{
			System.out.print(arr[i][j] + " ");
		}
		System.out.println();//換行
		}
	}
}


